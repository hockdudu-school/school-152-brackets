import $ from 'jquery';

const $navbarBurgerButton = $('.navbar-burger');
const $navbarList = $('.navbar-items');

$navbarBurgerButton.on('click', () => {
    if ($navbarList.hasClass('navbar-open')) {
        $navbarList.removeClass('navbar-open');
        window.removeEventListener('click', windowClickListener, true)
    } else {
        $navbarList.addClass('navbar-open');
        window.addEventListener('click', windowClickListener, true)
    }
});

function windowClickListener(event) {
    if ($navbarList.hasClass('navbar-open') && !$(event.target).closest('#navbar').length) {
        $navbarList.removeClass('navbar-open');
        window.removeEventListener('click', windowClickListener, true);
        event.preventDefault();
        event.stopImmediatePropagation();
    }
}

window.addEventListener('scroll', windowClickListener);
