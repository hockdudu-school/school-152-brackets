import Headroom from 'headroom.js';

const header = document.getElementById('header');
const headroom = new Headroom(header);

headroom.init();