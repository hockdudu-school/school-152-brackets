import Plyr from 'plyr';

import translation from './plyr_translation';

Array.from(document.querySelectorAll('.player')).map(p => new Plyr(p, {
    i18n: translation,
    tooltips: {
        controls: true,
        seek: true
    },
    disableContextMenu: false,
    controls: [
        'play-large',
        'play',
        'progress',
        'current-time',
        'mute',
        'volume',
        'captions',
        'settings',
        'pip',
        'airplay',
        'download',
        'fullscreen'
    ],
    urls: {
        download: p.dataset['download']
    },
    quality: {
        default: 720
    },
    youtube: {
        noCookie: true
    }
}));