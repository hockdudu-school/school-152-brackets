import $ from 'jquery';
import Beerslider from 'beerslider';

import translation from './gallery_translation';

const $body = $('body');

function openGallery($element, skipPushState = false) {
    if ($body.attr('data-gallery-open')) return;

    $body.attr('data-gallery-open', true);

    const $gallery = $element.closest('.gallery');
    let currentImageIndex = 0;

    const galleryImages = $gallery.find('.gallery-item').toArray().map((element, index) => {
        const $image = $(element);

        if ($image.is($element)) {
            currentImageIndex = index;
        }

        return {
            img: $image.data('src'),
            small: $image.data('thumb'),
            alt: $image.data('alt'),
            caption: $image.data('caption'),
            img_orig: $image.data('urlOrig'),
            download: $image.data('download'),
            _source: $image
        };
    });

    const $galleryViewer = $('<div>', {class: 'gallery-viewer'}).data('galleryId', $gallery.data('galleryId'));

    const $galleryPrev = $('<div/>', {class: 'gallery-prev', attr: {title: translation.previous}});
    const $galleryNext = $('<div/>', {class: 'gallery-next', attr: {title: translation.next}});
    const $galleryClose = $('<div/>', {class: 'gallery-close', attr: {title: translation.close}});
    const $galleryDownload = $('<div/>', {class: 'gallery-download', attr: {title: translation.download}});
    const $galleryHolder = $('<div>', {class: 'gallery-holder'});

    $galleryClose.on('click', () => closeGallery());

    $galleryViewer.append([$galleryClose, $galleryDownload, $galleryHolder]);
    if (galleryImages.length > 1) {
        $galleryViewer.append([$galleryPrev, $galleryNext]);
    }

    const keyEvents = (event) => {
        switch (event.key) {
            case 'Escape':
                closeGallery();
                break;
            case 'ArrowLeft':
                showPreviousImage();
                break;
            case 'ArrowRight':
                showNextImage();
                break;
        }
    };

    $(document).on('keyup', keyEvents);

    const buttonsSelect = 'figcaption, .gallery-prev, .gallery-next, .gallery-close, .gallery-download, .beer-handle';

    $galleryViewer.on('mousemove', event => {
        if (window.USER_IS_TOUCHING) {
            return;
        }

        $galleryViewer.removeClass('inactive');
        const currentMoveTimeoutId = $galleryViewer.data('moveTimeout');

        if (currentMoveTimeoutId) {
            clearTimeout(currentMoveTimeoutId);
        }

        if (!$(event.target).is(buttonsSelect)) {
            $galleryViewer.data('moveTimeout', setTimeout(() => {
                $galleryViewer.addClass('inactive');
            }, 5000));
        } else {
            $galleryViewer.data('moveTimeout', null);
        }
    });

    $galleryViewer.on('touchstart touchmove', event => {
        const currentMoveTimeoutId = $galleryViewer.data('moveTimeout');
        if (currentMoveTimeoutId) {
            clearTimeout(currentMoveTimeoutId);
        }

        if (event.type === 'touchmove') {
            if ($(event.target).is('.beer-range')) {
                $galleryViewer.addClass('inactive');
            }
        } else if (!$(event.target).is(buttonsSelect)) {
            $galleryViewer.toggleClass('inactive');
        }
    });

    $galleryViewer.trigger('mousemove');

    for (let image of galleryImages) {

        const $figure = $('<figure>', {class: 'image'}).appendTo($galleryHolder);
        if (image.caption) {
            $figure.append($('<figcaption>', {text: image.caption}));
        }

        if (!image.img_orig) {
            $figure.append($('<img>', {src: image.small}))
        } else {
            $figure.append($('<div>', {class: 'beer-slider'})
                .append($('<img>', {src: image.img_orig}))
                .append($('<div>', {class: 'beer-reveal'})
                    .append($('<img>', {src: image.img}))
                )
            );
        }
    }

    $galleryViewer.find('.beer-slider').each(function() {
        new Beerslider(this);
    });

    $galleryViewer.appendTo($body);
    $body.data('gallery', $galleryViewer);
    $galleryViewer.data('images', galleryImages);
    $galleryViewer.data('closeOnEscape', keyEvents);

    if (!skipPushState) {
        history.pushState({}, '', `#${$gallery.data('galleryId')}_${currentImageIndex}`);
    }

    showImage(currentImageIndex);
}

function showImage(index) {
    const $galleryViewer = $body.data('gallery');
    if (!$galleryViewer) return;

    const $figures = $galleryViewer.find('figure.image');
    $figures.removeClass('current');

    const $currentFigure = $figures.eq(index);
    $currentFigure.addClass('current');

    const images = $galleryViewer.data('images');
    const $currentImg = $currentFigure.find('> img');
    const imageData = images[index];

    if ($currentImg.attr('src') !== imageData.img) {
        const $preload = $currentImg.clone();
        $preload.attr('src', null);
        $preload.css('display', 'none');

        $preload.on('load', () => {
            $preload.fadeIn({
                duration: 200,
                complete: () => {
                    $currentImg.remove();
                }
            });
        });

        $preload.attr('src', imageData.img);
        $preload.insertAfter($currentImg);
    }

    const $downloadButton = $galleryViewer.find('.gallery-download');
    $downloadButton.off('click');

    if (imageData.download) {
        $downloadButton.on('click', () => {
            window.open(imageData.download);
        });
        $downloadButton.show();
    } else {
        $downloadButton.hide();
    }

    history.replaceState({}, '', `#${$galleryViewer.data('galleryId')}_${index}`);

    $galleryViewer.find('.gallery-prev').off().on('click', () => {
        showPreviousImage();
    });

    $galleryViewer.find('.gallery-next').off().on('click', () => {
        showNextImage();
    });

    // Fixes slider
    window.dispatchEvent(new Event('resize'));
}

function getCurrentImageIndex() {
    const $galleryViewer = $body.data('gallery');
    if (!$galleryViewer) return;

    const $figures = $galleryViewer.find('figure.image');
    return [$figures.index($figures.filter('.current')), $figures];
}

function showPreviousImage() {
    const [index, $figures] = getCurrentImageIndex();

    if (index < 0) {
        return;
    }

    showImage(index === 0 ? $figures.length - 1 : index - 1);
}

function showNextImage() {
    const [index, $figures] = getCurrentImageIndex();

    if (index < 0) {
        return;
    }

    showImage(index === $figures.length - 1 ? 0 : index + 1);
}

function closeGallery() {
    const $galleryViewer = $body.data('gallery');
    if (!$galleryViewer) return;

    $body.removeAttr('data-gallery-open');
    $galleryViewer.remove();

    $(document).off($galleryViewer.data('closeOnEscape'));

    history.replaceState({}, '', '#');
}

function locationUpdated() {
    const hash = document.location.hash;

    // if is empty
    if (!hash) {
        closeGallery();
        return;
    }

    const hashParseRegex = /#([^_]+)_(\d+)/;
    const result = hashParseRegex.exec(hash);

    // If hash couldn't be parsed
    if (!result) {
        return;
    }

    const galleryId = result[1];
    const galleryIndex = result[2];

    const $gallery = $(`.gallery[data-gallery-id=${galleryId}]`);

    // If gallery couldn't be found
    if (!$gallery.length) {
        return;
    }

    const $galleryNthElement = $gallery.find('.gallery-item').eq(galleryIndex);

    // If wanted index couldn't be found
    if (!$galleryNthElement.length) {
        return;
    }

    openGallery($galleryNthElement, true);
}

$('.gallery').find('.gallery-item-link').on('click', event => {
    event.preventDefault();
    const $galleryItem = $(event.currentTarget).find('.gallery-item');
    openGallery($galleryItem);
});

$(window).on('popstate', () => {
    locationUpdated();
});

// When the user reloads the page
locationUpdated();

// Whether the user is using touch or not
window.addEventListener('touchstart', function onFirstTouch() {
    window.USER_IS_TOUCHING = true;
    window.removeEventListener('touchstart', onFirstTouch);
});