const path = require('path');
const fs = require('fs');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const templatesLocation = './src/templates/';

const inputList = readFilesRecursively(templatesLocation, /^(?!_).*\.njk$/).map(file => {
    return new HtmlWebpackPlugin({
        filename: file.name.replace('.njk', '.html'),
        template: `${templatesLocation}${file.name}`
    });
});

module.exports = (env, argv) => ({
    entry: ['./src/js/app.js', './src/scss/main.scss'],
    output: {
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.njk$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            attrs: [
                                'img:src',
                                'a:href',
                                ':data-src',
                                ':data-thumb',
                                ':data-url-orig',
                                ':data-download',
                                'video:src',
                                'source:src',
                                'link:href',
                            ]
                        }
                    },
                    {
                        loader: 'nunjucks-html-loader',
                        options: {
                            searchPaths: [templatesLocation]
                        }
                    }
                ]
            }, {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpg)$/,
                use: [
                    {
                        loader: 'responsive-loader',
                        options: {
                            adapter: require('responsive-loader/sharp'),
                            publicPath: '/assets/',
                            outputPath: 'assets',
                            name: '[md5:contentHash:hex:4]-[name]-[width].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(pdf|svg|rw2|cr2|mp4|webm|mkv|avi)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: '/assets/',
                            outputPath: 'assets',
                            name: '[md5:contentHash:hex:4]-[name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        ...inputList
    ],
    resolve: {
        alias: {
            Pictures: path.resolve(__dirname, 'assets/pictures'),
            Doc: path.resolve(__dirname, 'assets/doc'),
            Img: path.resolve(__dirname, 'assets/img'),
            Raw: path.resolve(__dirname, 'assets/raw'),
            Videos: path.resolve(__dirname, 'assets/videos'),
        }
    }
});

function readFilesRecursively(entryPoint, searchRegex, scanSubdirectories = true) {
    const files = fs.readdirSync(entryPoint, {withFileTypes: true}).filter(file => {
        if (file.isFile()) {
            return searchRegex.test(file.name);
        } else if (file.isDirectory()) {
            return scanSubdirectories;
        } else {
            return false;
        }
    });

    if (scanSubdirectories) {
        for (const file of files) {
            if (file.isDirectory()) {
                const subFiles = readFilesRecursively(`${entryPoint}/${file.name}`, searchRegex, scanSubdirectories);
                subFiles.forEach(subFile => subFile.name = `${file.name}/${subFile.name}`);
                files.push(...subFiles);
            }
        }
    }

    return files.filter(file => file.isFile());
}