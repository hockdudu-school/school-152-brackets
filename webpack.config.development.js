const merge = require('webpack-merge');
const baseConfig = require('./webpack.config.base');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env, argv) =>
    merge(baseConfig(env, argv), {
        mode: 'development',
        output: {
            filename: 'app.js',
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css'
            }),
        ]
    });