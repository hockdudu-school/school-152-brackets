module.exports = (env, argv) => {
    if (!argv.hasOwnProperty('mode')) {
        argv.mode = 'production';
    }

    return require(`./webpack.config.${argv.mode}.js`)(env, argv);
};