FROM node:10-alpine

RUN npm config set unsafe-perm true

WORKDIR /build/
COPY package*.json /build/
RUN npm i

COPY . /build/
RUN npm run build

FROM nginx:alpine
COPY --from=0 /build/dist/ /usr/share/nginx/html/
